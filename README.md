# [10321] Accordion Shortcode #
**Contributors:** cgrymala

**Tags:** jquery-ui, accordion, shortcode, post types

**Requires at least:** 3.9.1

**Tested up to:** 4.1.1

**Stable tag:** 0.1.9

**License:** GPLv2 or later

**License URI:** http://www.gnu.org/licenses/gpl-2.0.html


Allows simple implementation of accordion-style interfaces

## Description ##

This plugin makes it easy to implement accordion-style interfaces on the front-end.

There are a few different ways to add an accordion.

# Inline Accordions #

If you'd like to create an accordion of content directly within a post or page, you can use a combination of three different shortcodes:

1. [accordion][/accordion] - This shortcode should be wrapped around the entire group of content that should be turned into an accordion
1. [accordion-title][/accordion-title] - This shortcode should be wrapped around each heading for accordion items
1. [accordion-content][/accordion-content] - This shortcode should be wrapped around each piece of content that needs to become an accordion item

An example might look like:

```
[accordion]
[accordion-title]Section 1[/accordion-title]
[accordion-content]This is some content that will appear inside of section 1[/accordion-content]
[accordion-title]Section 2[/accordion-title]
[accordion-content]This is some content that will appear inside of section 2[/accordion-content]
[/accordion]
```

# Aggregate Accordions #

You can also create accordions by automatically pulling in the titles & content from items stored elsewhere in your WordPress installation. For instance, if you'd like to create an accordion interface for your FAQ, you could create a custom post type for your FAQ questions, then use the `[accordion]` shortcode to pull in all FAQ questions. That might look something like the following:

```
[accordion post_type="question"]
```

As an aside, the aggregate accordion is configured to pull in child content by default. Therefore, if you don't set a `post_type` within the accordion shortcode, it will look for any child posts of the post where the accordion shortcode was inserted. This can be handy for easily displaying a list of titles/excerpts of child pages.

## Frequently Asked Questions ##

### Why are my inline accordions getting messed up? ###

There is a bit of an odd situation in the way WordPress handles new lines in shortcodes. Because of the way WordPress automatically wraps items in `<p></p>` tags when there are two line-breaks between them, you have to be careful how you separate the nested shortcodes.

One line-break (Shift+Enter when in the Visual Editor) should be fine between your nested shortcodes:

E.G.

```
[accordion]
[accordion-title]My Title[/accordion-title]
[accordion-content]My Content[/accordion-content]
[/accordion]
```

Two line-breaks (normal Enter in Visual Editor) will cause this issue.

E.G.

```
[accordion]

[accordion-title]My Title[/accordion-title]

[accordion-content]My content[/accordion-content]

[/accordion]
```

## Shortcode Attributes ##

There are a handful of attributes available to the main [accordion] shortcode.

1. `'post_type'   => $default_type, ` - What post type should the accordion pull in (only valid if using the aggregate accordion functionality)
1. `'orderby'     => 'menu_order title', ` - What meta information should be used to sort the content in the accordion (only valid if using the aggregate accordion functionality)
1. `'order'       => 'ASC', ` - What order should the posts be displayed in (only valid if using the aggregate accordion functionality)
1. `'posts_per_page' => -1, ` - How many pieces of content to display in the accordion (only valid if using the aggregate accordion functionality)
1. `'numberposts' => -1, ` - Can be used in place of `posts_per_page`
1. `'method'      => 'ui', ` - If set to `ui`, the jQuery UI accordion module will be used to create the accordion. If set to anything else, a more basic JS implementation will be used
1. `'closed'      => false, ` - Whether the accordion should begin with all items closed (currently only works with UI)
1. `'heightStyle' => 'auto', ` - See [jQuery UI Accordion heightStyle](http://api.jqueryui.com/accordion/#option-heightStyle) (currently only works with UI)
1. `'collapsible' => false, ` - See [jQuery UI Accordion collapsible](http://api.jqueryui.com/accordion/#option-collapsible) (currently only works with UI)
1. `'active'      => 0 ` - See [jQuery UI Accordion active](http://api.jqueryui.com/accordion/#option-active) (currently only works with UI)

## Changelog ##

### 0.1.9 ###

* Change main plugin file name to avoid clashes with public WP plugin repo
* Change plugin name in all files to avoid clashes with public WP plugin repo

### 0.1.7 ###

* Fix JS output for inline-accordion style

### 0.1.6 ###

* Switch from using hidden CSS class to using standard .hide()

### 0.1.2 ###

* Add inline-accordion layout to allow accordions to be completely self-contained within a piece of content
