<?php
/**
 * Plugin Name: [10321] Accordion Shortcode
 * Description: Implements a shortcode to generate accordions
 * Version: 0.1.9
 * Author: Curtiss Grymala
 * Author URI: http://ten-321.com/
 * License: GPL2
 */

if ( ! class_exists( 'Accordion_Shortcode' ) ) {
	class Accordion_Shortcode {
		public $accordion_id = 0;
		public $version = '0.1.9';
		
		/**
		 * Instantiate our class
		 */
		function __construct() {
			add_action( 'init', array( $this, 'register_shortcodes' ) );
		}
		
		/**
		 * Enqeueue any scripts that are needed for this plugin
		 */
		function enqueue_scripts() {
			wp_enqueue_script( 'accordion-shortcode', plugins_url( 'accordion-shortcode.js', __FILE__ ), array( 'jquery' ), $this->version, true );
		}
		
		/**
		 * Register any necessary shortcodes
		 */
		function register_shortcodes() {
			// Register the accordion shortcode
			add_shortcode( 'accordion', array( $this, 'accordion' ) );
			add_shortcode( 'accordion-title', array( $this, 'do_accordion_title_shortcode' ) );
			add_shortcode( 'accordion-content', array( $this, 'do_accordion_item_shortcode' ) );
		}

		/**
		 * Output an accordion of child elements
		 */
		function accordion( $atts=array(), $content=null ) {
			if ( ! empty( $content ) && has_shortcode( $content, 'accordion-content' ) ) {
				$content = preg_replace( '`\]\s*?<br( /{0,1})>\s*?\[`', '][', $content );
				$content = preg_replace( '`\]\s*?<p></p>\s*?\[`', '][', $content );
				$rt .= '
	<section class="shortcode-accordion">';
				$rt .= force_balance_tags( do_shortcode( $content ) );
				$rt .= '
	</section>';
	
				if ( $atts['method'] == 'ui' ) {
					wp_enqueue_script( 'jquery-ui-accordion' );
					$this->accordion_atts = array( 'collapsible' => $atts['collapsible'], 'heightStyle' => $atts['heightStyle'], 'active' => ( $atts['closed'] ? false : $atts['active'] ) );
					add_action( 'wp_footer', array( $this, 'ui_script' ), 99 );
				} else {
					$this->enqueue_scripts();
					add_action( 'wp_footer', array( $this, 'native_script' ), 99 );
				}
				
				return $rt;
			}
			
			global $post;
			if ( ! isset( $post ) || ! is_object( $post ) )
				return;
			
			$default_type = ( is_singular() ) ? get_post_type() : 'page';
			
			$defaults = array(
				'post_type'   => $default_type, 
				'orderby'     => 'menu_order title', 
				'order'       => 'ASC', 
				'posts_per_page' => -1, 
				'numberposts' => -1, 
				'method'      => 'ui', 
				'closed'      => false, 
				'heightStyle' => 'auto', 
				'collapsible' => false, 
				'active'      => 0
			);
			
			if ( empty( $atts ) )
				$defaults['post_parent'] = $post->ID;
			
			$atts = shortcode_atts( $defaults, $atts );
			
			if ( $atts['method'] == 'ui' ) {
				wp_enqueue_script( 'jquery-ui-accordion' );
				$this->accordion_atts = array( 'collapsible' => $atts['collapsible'], 'heightStyle' => $atts['heightStyle'], 'active' => ( $atts['closed'] ? false : $atts['active'] ) );
				add_action( 'wp_footer', array( $this, 'ui_script' ), 99 );
			} else {
				$this->enqueue_scripts();
				add_action( 'wp_footer', array( $this, 'native_script' ), 99 );
			}
			
			$children = new WP_Query( $atts );
			if ( ! $children->have_posts() )
				return;
			
			$rt = '
		<section class="shortcode-accordion">';
			while ( $children->have_posts() ) : $children->the_post();
				$rt .= '
			<h3 class="accordion-title"><a href="#post-' . get_the_ID() . '">' . apply_filters( 'the_title', get_the_title() ) . '</a></h3>
			<div class="accordion-content" id="post-' . get_the_ID() . '">' . apply_filters( 'the_content', get_the_content() ) . '</div>';
			endwhile;
			$rt .= '
		</section>';
		
			wp_reset_postdata();
		
			return $rt;
		}
		
		/**
		 * Handle the titles within nested accordion shortcodes
		 */
		function do_accordion_title_shortcode( $atts=array(), $content=null ) {
			return sprintf( '<h3 class="accordion-title"><a href="#accordion-section-%d">%s</a></h3>', $this->accordion_id, $content );
		}
		
		/**
		 * Handle the content within nested accordion shortcodes
		 */
		function do_accordion_item_shortcode( $atts=array(), $content=null ) {
			$rt = sprintf( '<div class="accordion-content" id="accordion-section-%d">%s</div>', $this->accordion_id, do_shortcode( $content ) );
			$this->accordion_id++;
			return $rt;
		}
		
		/**
		 * Output the necessary JavaScript to use the jQuery UI accordion
		 */
		function ui_script() {
?>
<script>
jQuery( '.shortcode-accordion' ).accordion( <?php echo json_encode( $this->accordion_atts ) ?> );
</script>
<?php
		}
		
		/**
		 * Output the necessary JavaScript to use the native accordion
		 */
		function native_script() {
			return;
		}
		
	} /* End Accordion_Shortcode class definition */
	
	global $accordion_obj;
	$accordion_obj = new Accordion_Shortcode;
} /* End if statement */