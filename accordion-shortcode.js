/**
 * Accordion Shortcode JavaScript
 * @package [10321] Accordion Shortcode
 * @version 0.1.9
 */
jQuery( function( $ ) {
	$( '.shortcode-accordion .accordion-content' ).addClass( 'hidden' ).hide();
	
	$( '.shortcode-accordion .accordion-title' ).click( function() {
		if ( $( this ).next().hasClass( 'hidden' ) ) {
			$( '.shortcode-accordion .accordion-content' ).addClass( 'hidden' ).hide();
			$( this ).next().removeClass( 'hidden' ).show();
			if ( $( 'body' ).hasClass( 'admin-bar' ) ) {
				$( 'html, body' ).animate( {
					scrollTop: ( $( this ).offset().top - 32 )
				}, 500 );
			} else {
				$( 'html, body' ).animate( {
					scrollTop: $( this ).offset().top
				}, 500 );
			}
		} else {
			$( '.shortcode-accordion .accordion-content' ).addClass( 'hidden' ).hide();
		}
		if ( $( '.hidden' ).is( ':visible' ) )
			return true;
		else
			return false;
	} );
	if ( location.hash && $( '.shortcode-accordion .accordion-content' ).length >= 1 ) {
		if ( $( '.shortcode-accordion ' + location.hash ).length > 0 ) {
			$( '.shortcode-accordion ' + location.hash ).removeClass( 'hidden' ).show();
			if ( $( 'body' ).hasClass( 'admin-bar' ) ) {
				$( 'html, body' ).animate( {
					scrollTop: ( $( location.hash ).offset().top - 32 )
				}, 500 );
			} else {
				$( 'html, body' ).animate( {
					scrollTop: $( location.hash ).offset().top
				}, 500 );
			}
		}
	}

} );